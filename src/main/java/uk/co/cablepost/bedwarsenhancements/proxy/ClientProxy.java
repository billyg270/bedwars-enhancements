package uk.co.cablepost.bedwarsenhancements.proxy;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.resources.model.ModelBakery;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.init.Items;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.model.obj.OBJLoader;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.*;
import uk.co.cablepost.bedwarsenhancements.BedwarsEnhancements;
import uk.co.cablepost.bedwarsenhancements.TestItem;

public class ClientProxy implements IProxy {
    /**
     * Called before the mod is fully initialized
     * <p>
     * Registries: Initiate variables and client command registries
     *
     * @param event Forge's pre-init event
     */
    @Override
    public void preInit(FMLPreInitializationEvent event) {
        OBJLoader.instance.addDomain(BedwarsEnhancements.MOD_ID);

        ModelLoader.setCustomModelResourceLocation(TestItem.INSTANCE, 0, new ModelResourceLocation(BedwarsEnhancements.MOD_ID + ":knife", "inventory"));
    }

    /**
     * Called when the mod has been fully initialized
     * <p>
     * Registries: Events and client-server command registries
     *
     * @param event Forge's init event
     */
    @Override
    public void init(FMLInitializationEvent event) {
        RenderItem renderItem = Minecraft.getMinecraft().getRenderItem();
        //renderItem.getItemModelMesher().register(Items.wooden_sword, 0, new ModelResourceLocation(BedwarsEnhancements.MOD_ID + ":knife", "inventory"));
        //renderItem.getItemModelMesher().register(TestItem.INSTANCE, 0, new ModelResourceLocation(BedwarsEnhancements.MOD_ID + ":knife", "inventory"));
    }

    /**
     * Called after the mod has been successfully initialized
     * <p>
     * Registries: Nothing
     *
     * @param event Forge's post init event
     */
    @Override
    public void postInit(FMLPostInitializationEvent event) {
    }

    /**
     * Called after {@link FMLServerAboutToStartEvent} and before {@link FMLServerStartedEvent}.
     * <p>
     * Registries: Server commands
     *
     * @param event Forge's server starting event
     */
    @Override
    public void serverStarting(FMLServerStartingEvent event) {
    }
}
