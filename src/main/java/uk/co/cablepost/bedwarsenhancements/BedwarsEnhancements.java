package uk.co.cablepost.bedwarsenhancements;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import uk.co.cablepost.bedwarsenhancements.proxy.IProxy;

@Mod(modid = "bedwarsenhancements", version = "0.0.1")
public class BedwarsEnhancements {
    public static String MOD_ID = "bedwarsenhancements";

    @SidedProxy(

        // Client side proxy
        clientSide = "uk.co.cablepost.bedwarsenhancements.proxy.ClientProxy",

        // Server side proxy
        serverSide = "uk.co.cablepost.bedwarsenhancements.proxy.ServerProxy"
    )
    private static IProxy PROXY;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        //GameRegistry.registerItem(TestItem.INSTANCE);
        PROXY.preInit(event);
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        PROXY.init(event);
    }

    @Mod.EventHandler
    public void onFMLPostInitialization(FMLPostInitializationEvent event) {
        PROXY.postInit(event);
    }

    @Mod.EventHandler
    public void onFMLServerStarting(FMLServerStartingEvent event) {
        PROXY.serverStarting(event);
    }
}
