package uk.co.cablepost.bedwarsenhancements;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class TestItem extends Item {
    public static final TestItem INSTANCE = new TestItem();

    public TestItem() {
        setCreativeTab(CreativeTabs.tabCombat);
        setUnlocalizedName(BedwarsEnhancements.MOD_ID + ":test");
        setRegistryName("test");
    }
}
